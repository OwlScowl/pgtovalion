package com.epitech.pgtovalion;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Date;
import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "Matchs", foreignKeys = {
        @ForeignKey(entity = Team.class, parentColumns = "id", childColumns = "idTeam1", onDelete = CASCADE),
        @ForeignKey(entity = Team.class, parentColumns = "id", childColumns = "idTeam2", onDelete = CASCADE)
        })
public class Match {
    public Match() {}
    public Match(int id1, int id2, String location) {pidTeam1 = id1; pidTeam2 = id2; pDate = 0; this.location = location; }

    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int match_uid;

    @NonNull
    @ColumnInfo(name = "idTeam1")
    private int pidTeam1;

    @NonNull
    @ColumnInfo(name = "idTeam2")
    private int pidTeam2;

    @NonNull
    @ColumnInfo(name = "DateTime")
    private long pDate;

    @NonNull
    @ColumnInfo(name = "Location")
    private String location;


    @NonNull
    public int getMatch_uid() {
        return match_uid;
    }

    public void setMatch_uid(@NonNull int uid) {
        this.match_uid = uid;
    }

    @NonNull
    public int getPidTeam1() {
        return pidTeam1;
    }

    public void setPidTeam1(@NonNull int pidTeam1) {
        this.pidTeam1 = pidTeam1;
    }

    @NonNull
    public int getPidTeam2() {
        return pidTeam2;
    }

    public void setPidTeam2(@NonNull int pidTeam2) {
        this.pidTeam2 = pidTeam2;
    }

    @NonNull
    public long getPDate() {
        return pDate;
    }

    public void setPDate(@NonNull long pDate) {
        this.pDate = pDate;
    }

    @NonNull
    public String getLocation() {
        return location;
    }

    public void setLocation(@NonNull String location) {
        this.location = location;
    }
}
