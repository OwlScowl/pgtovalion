package com.epitech.pgtovalion;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "Teams")
public class Team {
    public Team() {}
    public Team(String _teamName) {this.pTeamName = _teamName;}

    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int uid;

    @NonNull
    @ColumnInfo(name = "TeamName")
    private String pTeamName;

    //TODO: STORE TEAM IMG
    //getters + setters
    public void setUid(@NonNull int uid) {
        this.uid = uid;
    }
    public int getUid() {
        return uid;
    }
    public void setPTeamName(@NonNull String pTeamName) {
        this.pTeamName = pTeamName;
    }
    @NonNull
    public String getPTeamName() {
        return pTeamName;
    }
}
