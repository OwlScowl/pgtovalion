package com.epitech.pgtovalion;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface TeamDao {
    @Insert
    void insert(Team team);

    @Query("DELETE FROM Teams")
    void deleteAll();

    @Query("SELECT * from teams Order by TeamName ASC")
    LiveData<List<Team>> getAllTeams();
}
