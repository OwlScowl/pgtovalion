package com.epitech.pgtovalion;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

public class TeamViewModel extends AndroidViewModel {

    private TeamRepository mRepository;

    private LiveData<List<Team>> mAllTeams;

    public TeamViewModel (Application application) {
        super(application);
        mRepository = new TeamRepository(application);
        mAllTeams = mRepository.getAllTeams();
    }

    LiveData<List<Team>> getAllTeams() { return mAllTeams; }

    public void insert(Team team) { mRepository.insert(team); }
}
