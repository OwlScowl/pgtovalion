package com.epitech.pgtovalion;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface MatchDao {
    @Insert
    void insert(Match match);

    @Query("DELETE FROM Matchs")
    void deleteAllMatch();

    @Query("SELECT * from matchs")
    LiveData<List<Match>> getAllmatch();

    @Query("Select * from matchs WHERE idTeam1=:id AND idTeam2=:id")
    LiveData<List<Match>> findMatchesForUser(final int id);
}
