package com.epitech.pgtovalion;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class TeamRepository {

    private TeamDao mTeamDao;
    private LiveData<List<Team>> mAllTeams;

    TeamRepository(Application application) {
        TeamRoomDatabase db = TeamRoomDatabase.getDatabase(application);
        mTeamDao = db.teamDao();
        mAllTeams = mTeamDao.getAllTeams();
    }

    LiveData<List<Team>> getAllTeams() {
        return mAllTeams;
    }


    public void insert (Team word) {
        new insertAsyncTask(mTeamDao).execute(word);
    }

    private static class insertAsyncTask extends AsyncTask<Team, Void, Void> {

        private TeamDao mAsyncTaskDao;

        insertAsyncTask(TeamDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Team... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}