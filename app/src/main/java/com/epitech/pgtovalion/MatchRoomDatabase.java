package com.epitech.pgtovalion;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;


@Database(entities = {Match.class}, version = 1)
public abstract class MatchRoomDatabase extends RoomDatabase {

    public abstract MatchDao matchDao();

    private static volatile MatchRoomDatabase INSTANCE;


    static MatchRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MatchRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            MatchRoomDatabase.class, "match_database").addCallback(sMatchRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }


    private static RoomDatabase.Callback sMatchRoomDatabaseCallback =
            new RoomDatabase.Callback() {

                @Override
                public void onOpen(@NonNull SupportSQLiteDatabase db) {
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final MatchDao mDao;

        PopulateDbAsync(MatchRoomDatabase db) {
            mDao = db.matchDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            mDao.deleteAllMatch();
            Match ExempleMatch = new Match(1, 2, "Berlin");
            mDao.insert(ExempleMatch);
            /*
            Team word = new Team("Fnatic");
            mDao.insert(word);
            word = new Team("Invictus Gaming");
            mDao.insert(word);
            */

            return null;
        }
    }
}
